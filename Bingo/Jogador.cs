﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jogo
{
    public class Jogador
    {
        public string username { set; get; }
        public Bingo cartao { set; get; }


        public Jogador(string nome)
        {
            username = nome;
            cartao = new Bingo();
        }


        public void imprimeLinhaifCor(int i)
        {
            string linha;
            if (cartao.flags[i] == true)
            {
                switch (i)
                {
                    case 0:
                        Console.ForegroundColor = ConsoleColor.Cyan;
                        break;
                    case 1:
                        Console.ForegroundColor = ConsoleColor.Green;
                        break;
                    case 2:
                        Console.ForegroundColor = ConsoleColor.Magenta;
                        break;
                    case 3:
                        Console.ForegroundColor = ConsoleColor.Red;
                        break;
                    case 4:
                        Console.ForegroundColor = ConsoleColor.Yellow;
                        break;
                    case 5:
                        Console.ForegroundColor = ConsoleColor.Blue;
                        break;
                    default:
                        Console.ForegroundColor = ConsoleColor.White;
                        break;
                }
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.White;
            }
            linha = cartao.imprimeLinha(cartao.Linhas[i]);
            Console.Write("{0,18}", linha);
            Console.ForegroundColor = ConsoleColor.White;
        }

        public string imprimeDados()
        {
            return string.Format(" |{0}\n{1}", username, cartao.imprimeDados());
        }
        
    }
}
