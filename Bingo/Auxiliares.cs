﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Jogo
{
    public class Auxiliares
    {
        static private string saveScore = @"score.txt";
        static private string saveFavoritos = @"favoritos.txt";

        public static bool scoreExiste()
        {
            bool check = File.Exists(saveScore) ? true : false;
            return check;
        }

        public static bool favoritosExiste()
        {
            bool check = File.Exists(saveFavoritos) ? true : false;
            return check;
        }

        public static List<string> getFavoritos()
        {
            StreamReader readerFavoritos = new StreamReader(saveFavoritos);
            List<string> favoritos = new List<string>();

            try
            {
                while (!readerFavoritos.EndOfStream)
                {
                    favoritos.Add(readerFavoritos.ReadLine());
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Ocorreu um erro.");
            }
            finally
            {
                readerFavoritos.Close();
            }


            return favoritos.Distinct().ToList();
        }

        public static void writeFavoritos(List<string> favoritos)
        {
            StreamWriter writerFavoritos = new StreamWriter(saveFavoritos, true);
            List<string> writtenFavoritos = new List<string>();

            for (int i = 0; i < favoritos.Count; i++)
            {
                writerFavoritos.WriteLine(favoritos[i]);
            }
            writerFavoritos.Close();
        }

        public static List<string> getScore()
        {
            StreamReader readerScore = new StreamReader(saveScore);
            List<string> score = new List<string>();

            try
            {
                while (!readerScore.EndOfStream)
                {
                    score.Add(readerScore.ReadLine());
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Ocorreu um erro.");
            }
            finally
            {
                readerScore.Close();
            }
            return score;
        }

        public static void writeScore(List<string> score)
        {
            StreamWriter writerScore = new StreamWriter(saveScore, false);
            try
            {
                for (int i = 0; i < score.Count; i++)
                {
                    writerScore.WriteLine(score[i]);
                }
            }
            catch(Exception)
            {
                Console.WriteLine("Ooops");
            }
            finally
            {
                writerScore.Close();
            }
        }

      

        public static byte checkByte()
        {
            byte numero = 0;
            bool flag = true;
            do
            {
                try
                {
                    numero = byte.Parse(Console.ReadLine());
                    flag = true;
                }
                catch (Exception)
                {
                    Console.WriteLine("Valor incorrecto, digite novamente:");
                    flag = false;
                }
            } while (!flag);

            return numero;
        }

        public static void fechar()
        {
            Console.Clear();
            System.Threading.Thread.Sleep(2000);
            Environment.Exit(0);
        }
        
    }
    
}
