﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jogo
{
    public class Bingo
    {
        public List<int>[] Linhas { set; get; }
        public bool[] flags { set; get; }
        public string[] coresWin = { "Blue", "Red", "Yellow", "Green", "Magenta" };
        static Random ran;
        public Bingo()
        {
            Linhas = new List<int>[5];
            flags = new bool[5];
            for(byte i = 0; i < 5; i++)
            {
                flags[i] = false;
            }
            this.random25();
        }
        
        public string imprimeLinha(List<int> Lista)
        {
            string linha = "";
            foreach (int num in Lista)
            {
                if (num == 0)
                {
                    linha += string.Format("  X");
                }
                else if(num < 10)
                {
                    linha += string.Format("  {0}", num);
                }
                else
                {
                    linha += string.Format(" {0}", num);
                }
            }
            return linha;
        }

        public string imprimeDados()
        {
            return string.Format("{0}\n{1}\n{2}\n{3}\n{4}\n", imprimeLinha(Linhas[0]), imprimeLinha(Linhas[1]), imprimeLinha(Linhas[2]), imprimeLinha(Linhas[3]), imprimeLinha(Linhas[4]) );
        }

        public void random25()
        {
            Random ran = new Random();
            List<int> lista = new List<int>();
            int aleatorio = 0;
            for (byte i = 0; i < 26; i++)
            {
                bool repetido = false;
                do
                {
                    aleatorio = ran.Next(1, 99);
                    if (!lista.Any(x => x == aleatorio))
                    {
                        repetido = true;
                    }
                } while (!repetido);
                lista.Add(aleatorio);
            }

            Linhas[0] = lista.GetRange(0, 5);
            Linhas[1] = lista.GetRange(6, 5);
            Linhas[2] = lista.GetRange(11, 5);
            Linhas[3] = lista.GetRange(16, 5);
            Linhas[4] = lista.GetRange(21, 5);
        }

        public static void showUsers(List<Jogador> jogadores, List<int> sorteados)
        {
            Console.Clear();
            Console.WriteLine("\nNumeros sorteados:");
            string chaves = "";
            foreach (int chave in sorteados)
            {
                chaves += chave + " ";
            }
            Console.WriteLine(chaves);
            Console.WriteLine();
            for (int i = -1; i < 5; i++)
            {
                foreach (Jogador user in jogadores)
                {
                    string nome;
                    if (i == -1) {
                        if (user.username.Length > 12) {
                            nome = user.username.Substring(0, 10);
                            nome += "...";
                        }
                        else
                        {
                            nome = user.username;
                        }
                        Console.Write("{0,18}", nome);
                    } else {
                        user.imprimeLinhaifCor(i);
                    }
                }
                Console.WriteLine();
            }
        }

        public static void jogarBingo(List<Jogador> jogadores)
        {
            ran = new Random();
            List<int> sorteados = new List<int>();
            bool vencedor = false;
            string resposta;
            do
            {
                int aleatorio = randomBingo(sorteados);
                int score;
                sorteados.Add(aleatorio);
                showUsers(jogadores, sorteados);
                
                foreach (Jogador user in jogadores)
                {
                    for (byte i = 0; i < 5; i++)
                    {
                        if (user.cartao.flags[i] == false)
                        {
                            int numX = user.cartao.Linhas[i].IndexOf(aleatorio);
                            if (numX != -1) {
                                user.cartao.Linhas[i][numX] = 0;
                            }
                            
                            if (user.cartao.Linhas[i].All(x => x == 0))
                            {
                                user.cartao.flags[i] = true;
                                if (user.cartao.flags.All(x => x == true))
                                {
                                    score = 50;
                                    for(byte f =0; f<3; f++) { 
                                        showUsers(jogadores, sorteados);
                                        System.Threading.Thread.Sleep(500);
                                        Console.WriteLine("\n\n PARABENS " + user.username.ToUpper() + " FIZESTE BINGO! GANHASTE 50 PONTOS!");
                                        System.Threading.Thread.Sleep(1000);
                                    }
                                    System.Threading.Thread.Sleep(2000);
                                    historico(user.username, score);
                                }
                                do
                                {
                                    showUsers(jogadores, sorteados);
                                    Console.WriteLine("\n" + user.username + " completou uma linha. Deseja continuar para bingo? \ns | Sim     n | Não");
                                    resposta = Console.ReadLine();
                                } while (resposta.ToLower() != "s" && resposta.ToLower() != "n");
                                if(resposta == "n")
                                {
                                    score = user.cartao.flags.Count(x => x == true) * 5;
                                    for (byte f = 0; f < 3; f++)
                                    {
                                        showUsers(jogadores, sorteados);
                                        System.Threading.Thread.Sleep(500);
                                        Console.WriteLine("\n\n PARABENS " + user.username.ToUpper() + " GANHASTE " + score + " PONTOS!");
                                        System.Threading.Thread.Sleep(1000);
                                    }
                                    System.Threading.Thread.Sleep(2000);
                                    historico(user.username, score);
                                }
                            }
                        }
                        
                    }
                }
                System.Threading.Thread.Sleep(50);

            } while (!vencedor);
        }

        public static void historico(string user, int score)
        {
            Console.Clear();
            List<string> resultados = new List<string>();
            bool added = false;

            if (Auxiliares.scoreExiste())
            {
                resultados = Auxiliares.getScore();
                for (byte linha=0; linha<resultados.Count();linha++)
                {
                    string[] merge = resultados[linha].Split('|');
                    if(merge[1].ToLower().Replace(" ", "") == user.ToLower()) { 
                        score = int.Parse(merge[0]) + score;
                        resultados[linha] = string.Format("{0} | {1}", score, user);
                        added = true;
                        break;
                    }
                }
                if (!added) { resultados.Add(string.Format("{0} | {1}", score, user)); }
            }
            else
            {
                resultados.Add(string.Format("{0} | {1}", score, user));
            }

            resultados.Sort();
            resultados.Reverse();

            foreach (string linha in resultados)
            {
                Console.WriteLine("{0,-20}",linha);
            }

            Auxiliares.writeScore(resultados);
            System.Threading.Thread.Sleep(1000);
            Console.ReadKey();
            Auxiliares.fechar();
        }


        public static int randomBingo(List<int> sorteados)
        {
            int sorteado;
            do
            {
                sorteado = ran.Next(1, 99);
            } while (sorteados.Any(x => x == sorteado));
            Console.Clear();
            return sorteado;
        }


    }
}
