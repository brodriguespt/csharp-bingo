﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jogo
{
    class Program
    {
        static List<Jogador> jogadores = new List<Jogador>();
        enum mainM { Jogar = 1, Sair = 2 };
        enum menuJogadores { InserirNickname = 1, EscolherNickname = 2, Jogar = 3, Voltar = 4};

        #region menujogadores
        static byte MmenuJogadoresPrint()
        {
            byte count = 0;
            Console.Clear();
            string msg = "";
            if(jogadores.Count > 0) { msg = string.Format("Tem {0} jogadores", jogadores.Count()); }
            Console.WriteLine("Qual a opção pretendida? {0}", msg);
            foreach (menuJogadores val in Enum.GetValues(typeof(menuJogadores)))
            {
                Console.WriteLine(++count + " - " + val);
            }
            return Auxiliares.checkByte();
        }

        static void MmenuJogadores()
        {
            byte opcao;
            do
            {
                opcao = MmenuJogadoresPrint();
                int numJogadores = jogadores.Count();
                switch (opcao)
                {
                    case 1:
                        if (numJogadores != 5)
                        {
                            inserirNickname();
                        }
                        break;
                    case 2:
                        if (numJogadores != 5)
                        {
                            escolherNickname();
                        }
                        break;
                    case 3:
                        if (numJogadores != 0)
                        {
                            Bingo.jogarBingo(jogadores);
                        }                            
                        break;
                    case 4:
                        break;
                    default:
                        Console.WriteLine("Opção inválida!");
                        break;
                }
            } while (opcao != (byte)menuJogadores.Voltar);
        }

        static bool checkUserRepetido(string nome)
        {
            foreach(Jogador user in jogadores)
            {
                if(user.username.ToLower() == nome.ToLower())
                {
                    return false;
                }
            }
            return true;
        }

        static void inserirNickname()
        {
            string resposta;
            string nome;
            List<string> favs = new List<string>();
            do { 
                Console.WriteLine("Insira username");
                nome = Console.ReadLine();
                nome = nome.Replace(" ", "");
                nome = nome.Replace("|", "");
            } while (!checkUserRepetido(nome) && nome != null);
            jogadores.Add(new Jogador(nome));

            do
            {
                Console.WriteLine("Deseja guardar nos favoritos?\ns | Sim    n | Não");
                resposta = Console.ReadLine();
            } while (resposta.ToLower() != "s" && resposta.ToLower() != "n");
            if(resposta == "s")
            {
                favs.Add(nome);
                Console.WriteLine("Adicionou com sucesso aos favoritos.");
                System.Threading.Thread.Sleep(1000);
            }
            Auxiliares.writeFavoritos(favs);
        }

        static void escolherNickname()
        {
            Console.Clear();

            List<string> favs = new List<string>();
            byte resposta;
            if(Auxiliares.favoritosExiste())
            { 
                favs = Auxiliares.getFavoritos();
                if (favs.Count() < 1) {
                    Console.WriteLine("Sem favoritos.");
                    System.Threading.Thread.Sleep(1000);

                    return; }
                for (byte i = 0; i < favs.Count(); i++)
                {
                    Console.WriteLine(i+1 + " | " + favs[i]);
                }
                do
                {
                    resposta = Auxiliares.checkByte();
                } while (resposta < 1 || resposta > favs.Count());
                if (checkUserRepetido(favs[resposta - 1])) { 
                    jogadores.Add(new Jogador(favs[resposta-1]));
                }
                System.Threading.Thread.Sleep(1000);
            }

        }
        #endregion

        static byte mainMenu()
        {
            byte count = 0;
            Console.Clear();
            Console.WriteLine("Qual a opção pretendida?");
            foreach (mainM val in Enum.GetValues(typeof(mainM)))
            {
                Console.WriteLine(++count + " - " + val);
            }
            return Auxiliares.checkByte();
        }
        
        static void Main(string[] args)
        {
            byte opcao;
            do
            {
                opcao = mainMenu();
                switch (opcao)
                {
                    case 1:
                        MmenuJogadores();
                        break;
                    case 2:
                        break;
                    default:
                        Console.WriteLine("Opção inválida!");
                        break;
                }
            } while (opcao != (byte)mainM.Sair);
        }
    }
}
